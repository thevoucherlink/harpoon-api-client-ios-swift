Pod::Spec.new do |s|
  s.name = 'HarpoonApi'
  s.ios.deployment_target = '8.0'
  s.osx.deployment_target = '10.9'
  s.version = '1.2.30'
  s.source = https://bitbucket.org/thevoucherlink/harpoon-api-client-ios-swift
  s.authors = 'Harpoon Connect'
  s.license = 'Apache License, Version 2.0'
  s.homepage = 'https://bitbucket.org/thevoucherlink/harpoon-api-client-ios-swift'
  s.source_files = 'HarpoonApi/Classes/Swaggers/**/*.swift'
  s.dependency 'Alamofire', '~> 3.4.1'
end
