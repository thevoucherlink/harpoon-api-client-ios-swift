//
// HRPStripeInvoice.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPStripeInvoice: JSONEncodable {
    public var object: String?
    public var amountDue: Double?
    public var applicationFee: Double?
    public var attemptCount: Double?
    public var attempted: Bool?
    public var charge: String?
    public var closed: Bool?
    public var currency: String?
    public var date: Double?
    public var description: String?
    public var endingBalance: Double?
    public var forgiven: Bool?
    public var livemode: Bool?
    public var metadata: String?
    public var nextPaymentAttempt: Double?
    public var paid: Bool?
    public var periodEnd: Double?
    public var periodStart: Double?
    public var receiptNumber: String?
    public var subscriptionProrationDate: Double?
    public var subtotal: Double?
    public var tax: Double?
    public var taxPercent: Double?
    public var total: Double?
    public var webhooksDeliveredAt: Double?
    public var customer: String?
    public var discount: HRPStripeDiscount?
    public var statmentDescriptor: String?
    public var subscription: HRPStripeSubscription?
    public var lines: [HRPStripeInvoiceItem]?
    public var id: Double?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["object"] = self.object
        nillableDictionary["amountDue"] = self.amountDue
        nillableDictionary["applicationFee"] = self.applicationFee
        nillableDictionary["attemptCount"] = self.attemptCount
        nillableDictionary["attempted"] = self.attempted
        nillableDictionary["charge"] = self.charge
        nillableDictionary["closed"] = self.closed
        nillableDictionary["currency"] = self.currency
        nillableDictionary["date"] = self.date
        nillableDictionary["description"] = self.description
        nillableDictionary["endingBalance"] = self.endingBalance
        nillableDictionary["forgiven"] = self.forgiven
        nillableDictionary["livemode"] = self.livemode
        nillableDictionary["metadata"] = self.metadata
        nillableDictionary["nextPaymentAttempt"] = self.nextPaymentAttempt
        nillableDictionary["paid"] = self.paid
        nillableDictionary["periodEnd"] = self.periodEnd
        nillableDictionary["periodStart"] = self.periodStart
        nillableDictionary["receiptNumber"] = self.receiptNumber
        nillableDictionary["subscriptionProrationDate"] = self.subscriptionProrationDate
        nillableDictionary["subtotal"] = self.subtotal
        nillableDictionary["tax"] = self.tax
        nillableDictionary["taxPercent"] = self.taxPercent
        nillableDictionary["total"] = self.total
        nillableDictionary["webhooksDeliveredAt"] = self.webhooksDeliveredAt
        nillableDictionary["customer"] = self.customer
        nillableDictionary["discount"] = self.discount?.encodeToJSON()
        nillableDictionary["statmentDescriptor"] = self.statmentDescriptor
        nillableDictionary["subscription"] = self.subscription?.encodeToJSON()
        nillableDictionary["lines"] = self.lines?.encodeToJSON()
        nillableDictionary["id"] = self.id
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
