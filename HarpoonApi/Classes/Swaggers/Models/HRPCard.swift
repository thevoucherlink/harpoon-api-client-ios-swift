//
// HRPCard.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPCard: JSONEncodable {
    public var id: String?
    public var name: String?
    public var type: String?
    public var funding: String?
    public var lastDigits: String?
    public var exMonth: String?
    public var exYear: String?
    public var cardholderName: String?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["id"] = self.id
        nillableDictionary["name"] = self.name
        nillableDictionary["type"] = self.type
        nillableDictionary["funding"] = self.funding
        nillableDictionary["lastDigits"] = self.lastDigits
        nillableDictionary["exMonth"] = self.exMonth
        nillableDictionary["exYear"] = self.exYear
        nillableDictionary["cardholderName"] = self.cardholderName
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
