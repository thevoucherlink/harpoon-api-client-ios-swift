//
// HRPCatalogCategoryProduct.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPCatalogCategoryProduct: JSONEncodable {
    public var categoryId: Double?
    public var productId: Double?
    public var position: Double?
    public var catalogProduct: AnyObject?
    public var catalogCategory: AnyObject?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["categoryId"] = self.categoryId
        nillableDictionary["productId"] = self.productId
        nillableDictionary["position"] = self.position
        nillableDictionary["catalogProduct"] = self.catalogProduct
        nillableDictionary["catalogCategory"] = self.catalogCategory
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
