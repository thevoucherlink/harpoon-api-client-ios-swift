//
// HRPUrbanAirshipConfig.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPUrbanAirshipConfig: JSONEncodable {
    public enum HRPDevelopmentLogLevel: String { 
        case Undefined = "undefined"
        case None = "none"
        case Error = "error"
        case Warn = "warn"
        case Info = "info"
        case Debug = "debug"
        case Trace = "trace"
    }
    public enum HRPProductionLogLevel: String { 
        case Undefined = "undefined"
        case None = "none"
        case Error = "error"
        case Warn = "warn"
        case Info = "info"
        case Debug = "debug"
        case Trace = "trace"
    }
    public var developmentAppKey: String?
    public var developmentAppSecret: String?
    public var developmentLogLevel: HRPDevelopmentLogLevel?
    public var productionAppKey: String?
    public var productionAppSecret: String?
    public var productionLogLevel: HRPProductionLogLevel?
    public var inProduction: Bool?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["developmentAppKey"] = self.developmentAppKey
        nillableDictionary["developmentAppSecret"] = self.developmentAppSecret
        nillableDictionary["developmentLogLevel"] = self.developmentLogLevel?.rawValue
        nillableDictionary["productionAppKey"] = self.productionAppKey
        nillableDictionary["productionAppSecret"] = self.productionAppSecret
        nillableDictionary["productionLogLevel"] = self.productionLogLevel?.rawValue
        nillableDictionary["inProduction"] = self.inProduction
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
