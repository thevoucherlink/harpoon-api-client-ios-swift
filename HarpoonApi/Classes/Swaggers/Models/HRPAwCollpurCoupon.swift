//
// HRPAwCollpurCoupon.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPAwCollpurCoupon: JSONEncodable {
    public var id: Double?
    public var dealId: Double?
    public var purchaseId: Double?
    public var couponCode: String?
    public var status: String?
    public var couponDeliveryDatetime: NSDate?
    public var couponDateUpdated: NSDate?
    public var redeemedAt: NSDate?
    public var redeemedByTerminal: String?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["id"] = self.id
        nillableDictionary["dealId"] = self.dealId
        nillableDictionary["purchaseId"] = self.purchaseId
        nillableDictionary["couponCode"] = self.couponCode
        nillableDictionary["status"] = self.status
        nillableDictionary["couponDeliveryDatetime"] = self.couponDeliveryDatetime?.encodeToJSON()
        nillableDictionary["couponDateUpdated"] = self.couponDateUpdated?.encodeToJSON()
        nillableDictionary["redeemedAt"] = self.redeemedAt?.encodeToJSON()
        nillableDictionary["redeemedByTerminal"] = self.redeemedByTerminal
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
