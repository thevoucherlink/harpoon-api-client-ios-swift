//
// HRPCustomerLoginCredentials.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPCustomerLoginCredentials: JSONEncodable {
    /** Customer Email */
    public var email: String?
    /** Customer Password */
    public var password: String?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["email"] = self.email
        nillableDictionary["password"] = self.password
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
