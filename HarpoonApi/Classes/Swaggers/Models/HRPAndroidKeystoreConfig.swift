//
// HRPAndroidKeystoreConfig.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPAndroidKeystoreConfig: JSONEncodable {
    public var storePassword: String?
    public var keyAlias: String?
    public var keyPassword: String?
    public var hashKey: String?
    public var id: Double?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["storePassword"] = self.storePassword
        nillableDictionary["keyAlias"] = self.keyAlias
        nillableDictionary["keyPassword"] = self.keyPassword
        nillableDictionary["hashKey"] = self.hashKey
        nillableDictionary["id"] = self.id
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
