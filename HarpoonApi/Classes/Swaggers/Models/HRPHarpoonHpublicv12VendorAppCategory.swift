//
// HRPHarpoonHpublicv12VendorAppCategory.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPHarpoonHpublicv12VendorAppCategory: JSONEncodable {
    public var id: Double?
    public var appId: Double?
    public var vendorId: Double?
    public var categoryId: Double?
    public var isPrimary: Double?
    public var createdAt: NSDate?
    public var updatedAt: NSDate?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["id"] = self.id
        nillableDictionary["appId"] = self.appId
        nillableDictionary["vendorId"] = self.vendorId
        nillableDictionary["categoryId"] = self.categoryId
        nillableDictionary["isPrimary"] = self.isPrimary
        nillableDictionary["createdAt"] = self.createdAt?.encodeToJSON()
        nillableDictionary["updatedAt"] = self.updatedAt?.encodeToJSON()
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
