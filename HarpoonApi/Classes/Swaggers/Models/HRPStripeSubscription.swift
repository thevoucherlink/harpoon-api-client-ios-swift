//
// HRPStripeSubscription.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPStripeSubscription: JSONEncodable {
    public var applicationFeePercent: Double?
    public var cancelAtPeriodEnd: Bool?
    public var canceledAt: Double?
    public var currentPeriodEnd: Double?
    public var currentPeriodStart: Double?
    public var metadata: String?
    public var quantity: Double?
    public var start: Double?
    public var status: String?
    public var taxPercent: Double?
    public var trialEnd: Double?
    public var trialStart: Double?
    public var customer: String?
    public var discount: HRPStripeDiscount?
    public var plan: HRPStripePlan?
    public var object: String?
    public var endedAt: Double?
    public var id: Double?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["applicationFeePercent"] = self.applicationFeePercent
        nillableDictionary["cancelAtPeriodEnd"] = self.cancelAtPeriodEnd
        nillableDictionary["canceledAt"] = self.canceledAt
        nillableDictionary["currentPeriodEnd"] = self.currentPeriodEnd
        nillableDictionary["currentPeriodStart"] = self.currentPeriodStart
        nillableDictionary["metadata"] = self.metadata
        nillableDictionary["quantity"] = self.quantity
        nillableDictionary["start"] = self.start
        nillableDictionary["status"] = self.status
        nillableDictionary["taxPercent"] = self.taxPercent
        nillableDictionary["trialEnd"] = self.trialEnd
        nillableDictionary["trialStart"] = self.trialStart
        nillableDictionary["customer"] = self.customer
        nillableDictionary["discount"] = self.discount?.encodeToJSON()
        nillableDictionary["plan"] = self.plan?.encodeToJSON()
        nillableDictionary["object"] = self.object
        nillableDictionary["endedAt"] = self.endedAt
        nillableDictionary["id"] = self.id
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
