//
// HRPCustomerUpdateData.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPCustomerUpdateData: JSONEncodable {
    /** New first name for the Customer */
    public var firstName: String?
    /** New last name for the Customer */
    public var lastName: String?
    /** New email for the Customer */
    public var email: String?
    /** New gender for the Customer */
    public var gender: String?
    /** New date of birth for the Customer */
    public var dob: String?
    /** New metadata collection for the Customer */
    public var metadata: AnyObject?
    /** New privacy settings for the Customer */
    public var privacy: HRPCustomerPrivacy?
    /** New profile picture for the Customer */
    public var profilePictureUpload: HRPMagentoImageUpload?
    /** New cover photo for the customer */
    public var coverUpload: HRPMagentoImageUpload?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["firstName"] = self.firstName
        nillableDictionary["lastName"] = self.lastName
        nillableDictionary["email"] = self.email
        nillableDictionary["gender"] = self.gender
        nillableDictionary["dob"] = self.dob
        nillableDictionary["metadata"] = self.metadata
        nillableDictionary["privacy"] = self.privacy?.encodeToJSON()
        nillableDictionary["profilePictureUpload"] = self.profilePictureUpload?.encodeToJSON()
        nillableDictionary["coverUpload"] = self.coverUpload?.encodeToJSON()
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
