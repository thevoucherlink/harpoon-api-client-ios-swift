//
// HRPContact.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPContact: JSONEncodable {
    /** Email address like hello@harpoonconnect.com */
    public var email: String?
    /** URL like http://harpoonconnect.com or https://www.harpoonconnect.com */
    public var website: String?
    /** Telephone number like 08123456789 */
    public var phone: String?
    public var address: HRPAddress?
    /** URL starting with https://www.twitter.com/ followed by your username or id */
    public var twitter: String?
    /** URL starting with https://www.facebook.com/ followed by your username or id */
    public var facebook: String?
    /** Telephone number including country code like: +35381234567890 */
    public var whatsapp: String?
    /** SnapChat username */
    public var snapchat: String?
    /** URL starting with https://plus.google.com/+ followed by your username or id */
    public var googlePlus: String?
    /** LinkedIn profile URL */
    public var linkedIn: String?
    /** A string starting with # and followed by alphanumeric characters (both lower and upper case) */
    public var hashtag: String?
    public var text: String?
    public var id: Double?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["email"] = self.email
        nillableDictionary["website"] = self.website
        nillableDictionary["phone"] = self.phone
        nillableDictionary["address"] = self.address?.encodeToJSON()
        nillableDictionary["twitter"] = self.twitter
        nillableDictionary["facebook"] = self.facebook
        nillableDictionary["whatsapp"] = self.whatsapp
        nillableDictionary["snapchat"] = self.snapchat
        nillableDictionary["googlePlus"] = self.googlePlus
        nillableDictionary["linkedIn"] = self.linkedIn
        nillableDictionary["hashtag"] = self.hashtag
        nillableDictionary["text"] = self.text
        nillableDictionary["id"] = self.id
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
