//
// HRPAnonymousModel8.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPAnonymousModel8: JSONEncodable {
    public var brandId: Double?
    public var competitionId: Double?
    public var couponId: Double?
    public var customerId: Double?
    public var eventId: Double?
    public var dealPaid: Double?
    public var dealGroup: Double?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["brandId"] = self.brandId
        nillableDictionary["competitionId"] = self.competitionId
        nillableDictionary["couponId"] = self.couponId
        nillableDictionary["customerId"] = self.customerId
        nillableDictionary["eventId"] = self.eventId
        nillableDictionary["dealPaid"] = self.dealPaid
        nillableDictionary["dealGroup"] = self.dealGroup
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
