//
// HRPAddress.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPAddress: JSONEncodable {
    /** Main street line for the address (first line) */
    public var streetAddress: String?
    /** Line to complete the address (second line) */
    public var streetAddressComp: String?
    public var city: String?
    /** Region, County, Province or State */
    public var region: String?
    /** Country ISO code (must be max of 2 capital letter) */
    public var countryCode: String?
    public var postcode: String?
    /** Postal Attn {person first name} */
    public var attnFirstName: String?
    /** Postal Attn {person last name} */
    public var attnLastName: String?
    public var id: Double?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["streetAddress"] = self.streetAddress
        nillableDictionary["streetAddressComp"] = self.streetAddressComp
        nillableDictionary["city"] = self.city
        nillableDictionary["region"] = self.region
        nillableDictionary["countryCode"] = self.countryCode
        nillableDictionary["postcode"] = self.postcode
        nillableDictionary["attnFirstName"] = self.attnFirstName
        nillableDictionary["attnLastName"] = self.attnLastName
        nillableDictionary["id"] = self.id
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
