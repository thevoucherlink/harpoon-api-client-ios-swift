//
// HRPCheckoutAgreement.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPCheckoutAgreement: JSONEncodable {
    public var name: String?
    public var contentHeight: String?
    public var checkboxText: String?
    public var isActive: Double?
    public var isHtml: Double?
    public var id: Double?
    public var content: String?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["name"] = self.name
        nillableDictionary["contentHeight"] = self.contentHeight
        nillableDictionary["checkboxText"] = self.checkboxText
        nillableDictionary["isActive"] = self.isActive
        nillableDictionary["isHtml"] = self.isHtml
        nillableDictionary["id"] = self.id
        nillableDictionary["content"] = self.content
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
