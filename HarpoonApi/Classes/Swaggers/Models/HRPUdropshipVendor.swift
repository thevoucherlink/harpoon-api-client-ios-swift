//
// HRPUdropshipVendor.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPUdropshipVendor: JSONEncodable {
    public var id: Double?
    public var vendorName: String?
    public var vendorAttn: String?
    public var email: String?
    public var street: String?
    public var city: String?
    public var zip: String?
    public var countryId: String?
    public var regionId: Double?
    public var region: String?
    public var telephone: String?
    public var fax: String?
    public var status: Bool?
    public var password: String?
    public var passwordHash: String?
    public var passwordEnc: String?
    public var carrierCode: String?
    public var notifyNewOrder: Double?
    public var labelType: String?
    public var testMode: Double?
    public var handlingFee: String?
    public var upsShipperNumber: String?
    public var customDataCombined: String?
    public var customVarsCombined: String?
    public var emailTemplate: Double?
    public var urlKey: String?
    public var randomHash: String?
    public var createdAt: NSDate?
    public var notifyLowstock: Double?
    public var notifyLowstockQty: String?
    public var useHandlingFee: Double?
    public var useRatesFallback: Double?
    public var allowShippingExtraCharge: Double?
    public var defaultShippingExtraChargeSuffix: String?
    public var defaultShippingExtraChargeType: String?
    public var defaultShippingExtraCharge: String?
    public var isExtraChargeShippingDefault: Double?
    public var defaultShippingId: Double?
    public var billingUseShipping: Double?
    public var billingEmail: String?
    public var billingTelephone: String?
    public var billingFax: String?
    public var billingVendorAttn: String?
    public var billingStreet: String?
    public var billingCity: String?
    public var billingZip: String?
    public var billingCountryId: String?
    public var billingRegionId: Double?
    public var billingRegion: String?
    public var subdomainLevel: Double?
    public var updateStoreBaseUrl: Double?
    public var confirmation: String?
    public var confirmationSent: Double?
    public var rejectReason: String?
    public var backorderByAvailability: Double?
    public var useReservedQty: Double?
    public var tiercomRates: String?
    public var tiercomFixedRule: String?
    public var tiercomFixedRates: String?
    public var tiercomFixedCalcType: String?
    public var tiershipRates: String?
    public var tiershipSimpleRates: String?
    public var tiershipUseV2Rates: Double?
    public var vacationMode: Double?
    public var vacationEnd: NSDate?
    public var vacationMessage: String?
    public var udmemberLimitProducts: Double?
    public var udmemberProfileId: Double?
    public var udmemberProfileRefid: String?
    public var udmemberMembershipCode: String?
    public var udmemberMembershipTitle: String?
    public var udmemberBillingType: String?
    public var udmemberHistory: String?
    public var udmemberProfileSyncOff: Double?
    public var udmemberAllowMicrosite: Double?
    public var udprodTemplateSku: String?
    public var vendorTaxClass: String?
    public var catalogProducts: [AnyObject]?
    public var vendorLocations: [AnyObject]?
    public var config: AnyObject?
    public var partners: [AnyObject]?
    public var udropshipVendorProducts: [AnyObject]?
    public var harpoonHpublicApplicationpartners: [AnyObject]?
    public var harpoonHpublicv12VendorAppCategories: [AnyObject]?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["id"] = self.id
        nillableDictionary["vendorName"] = self.vendorName
        nillableDictionary["vendorAttn"] = self.vendorAttn
        nillableDictionary["email"] = self.email
        nillableDictionary["street"] = self.street
        nillableDictionary["city"] = self.city
        nillableDictionary["zip"] = self.zip
        nillableDictionary["countryId"] = self.countryId
        nillableDictionary["regionId"] = self.regionId
        nillableDictionary["region"] = self.region
        nillableDictionary["telephone"] = self.telephone
        nillableDictionary["fax"] = self.fax
        nillableDictionary["status"] = self.status
        nillableDictionary["password"] = self.password
        nillableDictionary["passwordHash"] = self.passwordHash
        nillableDictionary["passwordEnc"] = self.passwordEnc
        nillableDictionary["carrierCode"] = self.carrierCode
        nillableDictionary["notifyNewOrder"] = self.notifyNewOrder
        nillableDictionary["labelType"] = self.labelType
        nillableDictionary["testMode"] = self.testMode
        nillableDictionary["handlingFee"] = self.handlingFee
        nillableDictionary["upsShipperNumber"] = self.upsShipperNumber
        nillableDictionary["customDataCombined"] = self.customDataCombined
        nillableDictionary["customVarsCombined"] = self.customVarsCombined
        nillableDictionary["emailTemplate"] = self.emailTemplate
        nillableDictionary["urlKey"] = self.urlKey
        nillableDictionary["randomHash"] = self.randomHash
        nillableDictionary["createdAt"] = self.createdAt?.encodeToJSON()
        nillableDictionary["notifyLowstock"] = self.notifyLowstock
        nillableDictionary["notifyLowstockQty"] = self.notifyLowstockQty
        nillableDictionary["useHandlingFee"] = self.useHandlingFee
        nillableDictionary["useRatesFallback"] = self.useRatesFallback
        nillableDictionary["allowShippingExtraCharge"] = self.allowShippingExtraCharge
        nillableDictionary["defaultShippingExtraChargeSuffix"] = self.defaultShippingExtraChargeSuffix
        nillableDictionary["defaultShippingExtraChargeType"] = self.defaultShippingExtraChargeType
        nillableDictionary["defaultShippingExtraCharge"] = self.defaultShippingExtraCharge
        nillableDictionary["isExtraChargeShippingDefault"] = self.isExtraChargeShippingDefault
        nillableDictionary["defaultShippingId"] = self.defaultShippingId
        nillableDictionary["billingUseShipping"] = self.billingUseShipping
        nillableDictionary["billingEmail"] = self.billingEmail
        nillableDictionary["billingTelephone"] = self.billingTelephone
        nillableDictionary["billingFax"] = self.billingFax
        nillableDictionary["billingVendorAttn"] = self.billingVendorAttn
        nillableDictionary["billingStreet"] = self.billingStreet
        nillableDictionary["billingCity"] = self.billingCity
        nillableDictionary["billingZip"] = self.billingZip
        nillableDictionary["billingCountryId"] = self.billingCountryId
        nillableDictionary["billingRegionId"] = self.billingRegionId
        nillableDictionary["billingRegion"] = self.billingRegion
        nillableDictionary["subdomainLevel"] = self.subdomainLevel
        nillableDictionary["updateStoreBaseUrl"] = self.updateStoreBaseUrl
        nillableDictionary["confirmation"] = self.confirmation
        nillableDictionary["confirmationSent"] = self.confirmationSent
        nillableDictionary["rejectReason"] = self.rejectReason
        nillableDictionary["backorderByAvailability"] = self.backorderByAvailability
        nillableDictionary["useReservedQty"] = self.useReservedQty
        nillableDictionary["tiercomRates"] = self.tiercomRates
        nillableDictionary["tiercomFixedRule"] = self.tiercomFixedRule
        nillableDictionary["tiercomFixedRates"] = self.tiercomFixedRates
        nillableDictionary["tiercomFixedCalcType"] = self.tiercomFixedCalcType
        nillableDictionary["tiershipRates"] = self.tiershipRates
        nillableDictionary["tiershipSimpleRates"] = self.tiershipSimpleRates
        nillableDictionary["tiershipUseV2Rates"] = self.tiershipUseV2Rates
        nillableDictionary["vacationMode"] = self.vacationMode
        nillableDictionary["vacationEnd"] = self.vacationEnd?.encodeToJSON()
        nillableDictionary["vacationMessage"] = self.vacationMessage
        nillableDictionary["udmemberLimitProducts"] = self.udmemberLimitProducts
        nillableDictionary["udmemberProfileId"] = self.udmemberProfileId
        nillableDictionary["udmemberProfileRefid"] = self.udmemberProfileRefid
        nillableDictionary["udmemberMembershipCode"] = self.udmemberMembershipCode
        nillableDictionary["udmemberMembershipTitle"] = self.udmemberMembershipTitle
        nillableDictionary["udmemberBillingType"] = self.udmemberBillingType
        nillableDictionary["udmemberHistory"] = self.udmemberHistory
        nillableDictionary["udmemberProfileSyncOff"] = self.udmemberProfileSyncOff
        nillableDictionary["udmemberAllowMicrosite"] = self.udmemberAllowMicrosite
        nillableDictionary["udprodTemplateSku"] = self.udprodTemplateSku
        nillableDictionary["vendorTaxClass"] = self.vendorTaxClass
        nillableDictionary["catalogProducts"] = self.catalogProducts?.encodeToJSON()
        nillableDictionary["vendorLocations"] = self.vendorLocations?.encodeToJSON()
        nillableDictionary["config"] = self.config
        nillableDictionary["partners"] = self.partners?.encodeToJSON()
        nillableDictionary["udropshipVendorProducts"] = self.udropshipVendorProducts?.encodeToJSON()
        nillableDictionary["harpoonHpublicApplicationpartners"] = self.harpoonHpublicApplicationpartners?.encodeToJSON()
        nillableDictionary["harpoonHpublicv12VendorAppCategories"] = self.harpoonHpublicv12VendorAppCategories?.encodeToJSON()
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
