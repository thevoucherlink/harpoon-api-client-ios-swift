//
// HRPCustomer.swift
//
// Generated by swagger-codegen
// https://github.com/swagger-api/swagger-codegen
//

import Foundation


public class HRPCustomer: JSONEncodable {
    public var email: String?
    public var firstName: String?
    public var lastName: String?
    public var dob: String?
    public var gender: String?
    public var password: String?
    public var profilePicture: String?
    public var cover: String?
    public var profilePictureUpload: HRPMagentoImageUpload?
    public var coverUpload: HRPMagentoImageUpload?
    public var metadata: AnyObject?
    public var connection: HRPCustomerConnection?
    public var privacy: HRPCustomerPrivacy?
    public var followingCount: Double?
    public var followerCount: Double?
    public var isFollowed: Bool?
    public var isFollower: Bool?
    public var notificationCount: Double?
    public var badge: HRPCustomerBadge?
    public var authorizationCode: String?
    public var id: Double?
    public var appId: String?

    public init() {}

    // MARK: JSONEncodable
    func encodeToJSON() -> AnyObject {
        var nillableDictionary = [String:AnyObject?]()
        nillableDictionary["email"] = self.email
        nillableDictionary["firstName"] = self.firstName
        nillableDictionary["lastName"] = self.lastName
        nillableDictionary["dob"] = self.dob
        nillableDictionary["gender"] = self.gender
        nillableDictionary["password"] = self.password
        nillableDictionary["profilePicture"] = self.profilePicture
        nillableDictionary["cover"] = self.cover
        nillableDictionary["profilePictureUpload"] = self.profilePictureUpload?.encodeToJSON()
        nillableDictionary["coverUpload"] = self.coverUpload?.encodeToJSON()
        nillableDictionary["metadata"] = self.metadata
        nillableDictionary["connection"] = self.connection?.encodeToJSON()
        nillableDictionary["privacy"] = self.privacy?.encodeToJSON()
        nillableDictionary["followingCount"] = self.followingCount
        nillableDictionary["followerCount"] = self.followerCount
        nillableDictionary["isFollowed"] = self.isFollowed
        nillableDictionary["isFollower"] = self.isFollower
        nillableDictionary["notificationCount"] = self.notificationCount
        nillableDictionary["badge"] = self.badge?.encodeToJSON()
        nillableDictionary["authorizationCode"] = self.authorizationCode
        nillableDictionary["id"] = self.id
        nillableDictionary["appId"] = self.appId
        let dictionary: [String:AnyObject] = APIHelper.rejectNil(nillableDictionary) ?? [:]
        return dictionary
    }
}
